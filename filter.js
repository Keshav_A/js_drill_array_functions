export function filter_callback(element) {
  return element > 1 ? element : false;
}

export function filter(elements, callback) {
    if (!Array.isArray(elements)) {
        console.log("Not an array, Provide array as an argument");
        return;
    }
    if (elements.length < 1) {
        console.log("Empty Array");
        return;
    }
    if (typeof callback !== "function") {
        console.log("Provide callback function as an argument");
        return;
    }
    const array = [];
    for (let index = 0; index < elements.length; index++) {
        if (callback(elements[index])) {
        array.push(elements[index]);
        }
    }
    return array;
}
