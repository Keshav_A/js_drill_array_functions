export function each_callback(value, index) {
    console.log (value + " is at index " + index);
    
}

export function each(elements, callback) {
    if (!Array.isArray(elements)) {
        console.log("Not an array, Provide array as an argument");
        return;
    }
    if (elements.length < 1) {
        console.log("Empty Array");
        return;
    }
    if (typeof callback !== "function") {
        console.log("Provide function as an argument");
        return;
    }
    for (let index = 0; index < elements.length; index++) {
        callback(elements[index], index);
    }
    return;
}
