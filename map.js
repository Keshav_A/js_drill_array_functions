export function callback(value) {
    return value;
}

export function map(elements, callback) {
    if (!Array.isArray(elements)) {
        console.log("Not an array, Provide array as an argument");
        return;
    }
    if (elements.length < 1) {
        console.log("Empty Array");
        return;
    }
    if (typeof callback !== "function") {
        console.log("Provide callback function as an argument");
        return;
    }
    const new_array = [];
    for (let index = 0; index < elements.length; index++) {
        if (typeof elements[index] !== "undefined") {
        new_array.push(callback(elements[index]));
        } else {
        new_array.push(`<1 empty item>`);
        }
    }
    if (new_array.length > 0) {
        return new_array;
    } else {
        return undefined;
    }
}
