export function reduce_callback(startingValue, element) {
    return startingValue + element;
}
export function reduce(elements, callback, startingValue) {
    if (!Array.isArray(elements)) {
        console.log("Not an array, Provide array as an argument");
        return;
    }
    if (elements.length < 1) {
        console.log("Empty Array");
        return;
    }
    if (typeof callback !== "function") {
        console.log("Provide function as second argument");
        return;
    }
    let index;
    if (!startingValue) {
        startingValue = elements[0];
        index = 1;
    } else {
        index = 0;
    }
    for (; index < elements.length; index++) {
        startingValue = callback(startingValue, elements[index]);
    }
    return startingValue;
}
