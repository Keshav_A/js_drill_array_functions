export function find_callback(element){
   
    if (element===5){
        return element;
    }
    else{
        return false;
    }
}
export function find(elements,callback){
    if (!Array.isArray(elements)) {
        console.log("Not an array, Provide array as an argument");
        return;
    }
    if (elements.length < 1) {
        console.log("Empty Array");
        return;
    }
    if (typeof callback !== "function") {
        console.log("Provide callback function as an argument");
        return;
    }
    let value = undefined
    for (let index =0 ;index < elements.length; index ++){
        if(callback(elements[index])){
            return callback(elements[index]);
        }

    }
    return "Element not present";

}