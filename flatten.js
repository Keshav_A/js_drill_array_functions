export function flatten(elements) {
    if (!Array.isArray(elements)) {
        console.log("Not an array, Provide array as an argument");
        return;
    }
    if (elements.length < 1) {
        console.log("Empty Array");
        return;
    }
    const flat_array = [];
    function nested(array) {
        for (let index = 0; index < array.length; index++) {
        if (typeof array[index] === "object") {
            nested(array[index]);
        } else {
            flat_array.push(array[index]);
        }
        }
    }
    nested(elements);
    return flat_array;
}
