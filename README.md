# JS Drill Arrays
Build functions to work with array

## Restrictions
- Recreate the functions from scratch.
- **DONT** Use for example .forEach() to recreate each, and .map() to recreate map etc.
- You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating

## File Naming 
Name your files like so:

`each.js`

`testEach.js`

`map.js`

`testMap.js`
## Problems
1. Do **NOT** use `.forEach()` to complete this function. 
    ```
    function each(elements, cb) {
        // Iterates over a list of elements, yielding each in turn to the `cb` function.
        // This only needs to work with arrays.
        // You should also pass the index into `cb` as the second argument based off 
        // http://underscorejs.org/#each
    }
    ```
2. Do **NOT** use `.map()`, to complete this function.
    ```
    function map(elements, cb) {
        // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
        // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
        // Return the new array.
    }
    ```
3. Do **NOT** use `.reduce()` to complete this function.
    ```
    function reduce(elements, cb, startingValue) {
        // How reduce works: A reduce function combines all elements into a single value going from left to right.
        // Elements will be passed one by one into `cb` along with the `startingValue`.
        // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
        // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    }
    ```
4. Do **NOT** use `.includes()`, to complete this function.
    ```
    function find(elements, cb) {
        // Look through each value in `elements` and pass each element to `cb`.
        // If `cb` returns `true` then return that element.
        // Return `undefined` if no elements pass the truth test.
    }
    ```
5. Do **NOT** use `.filter()`, to complete this function.
    ```
    function filter(elements, cb) {
        // Similar to `find` but you will return an array of all elements that passed the truth test
        // Return an empty array if no elements pass the truth test
    }
    ```
6. Flatten a nested array (the nesting can be to any depth).

    ```
    const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
    ```

    ```
    function flatten(elements) {
        // Flattens a nested array (the nesting can be to any depth).
        // Hint: You can solve this using recursion.
        // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    }
    ```


