import { items } from "../data.js";
import { filter_callback } from "../filter.js";
import { filter } from "../filter.js";

// filter_callback function returns an array with elements greater than 1

// Test 1

let array = filter({}, filter_callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns "undefined"

// Test 2

array = filter([], filter_callback);
// Returns "Empty Array"
console.log(array);
// Returns "undefined"

// Test 3

array = filter([]);
// Returns "Empty Array"
console.log(array);
// Returns "undefined"

// Test 4

array = filter(filter_callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns "undefined"

// Test 5

array = filter(items);
// Returns "Provide callback function as an argument"
console.log(array);
// Returns "undefined"

// Test 6

array = filter(items, "function");
// Returns "Provide callback function as an argument"
console.log(array);
// Returns "undefined"

// Test 7

array = filter(items, filter_callback);
console.log(array);
// Returns
// [2, 3, 4, 5, 5]

// Test 8

array = filter([1, 2, 3, "undefined", 4, 5], filter_callback);
console.log(array);
// Returns
// [2, 3, 4, 5]