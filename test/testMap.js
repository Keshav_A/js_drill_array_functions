import { items } from "../data.js";
import { map } from "../map.js";
import { callback } from "../map.js";

let array = map({}, callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns Undefined

array = map([], callback);
// Returns "Empty Array"
console.log(array);
// Returns Undefined

array = map([]);
// Returns "Empty Array"
console.log(array);
// Returns Undefined

array = map(callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns Undefined

array = map(items);
// Returns Provide function as an argument
console.log(array);
// Returns Undefined

array = map(items, "function");
console.log(array);
// Returns Undefined

array = map(items, callback);
console.log(array);
// Returns
// [1, 2, 3, 4, 5, 5]

array = map([1, 2, 3, "undefined", 4, 5], callback);
console.log(array);
// Returns
// [1, 2, 3, 'undefined', 4, 5]
