import { items } from "../data.js";
import { each_callback } from "../each.js";
import { each } from "../each.js";

const array1 = [1, 2, 3, , 4];
const array2 = [1, 2, undefined, NaN, Infinity, "s", { 1: 2, 3: 4 }];

// Test 1
each();
// Returns "Not an array, Provide array as an argument"

// Test 2
each(items);
// Returns "Provide functionas an argument"

// Test 3
each([]);
// Returns "Empty Array"

// Test 4
each({}, items);
// Returns "Not an array, Provide array as an argument"

// Test 5
each([], each_callback);
// Returns "Empty Array"

// Test 6
each([], items);
// Returns "Empty Array"

each(items, each_callback);
// Returns
// 1 is at index 0
// 2 is at index 1
// 3 is at index 2
// 4 is at index 3
// 5 is at index 4
// 5 is at index 5

each(array1, each_callback);
// Returns 
// 1 is at index 0
// 2 is at index 1
// 3 is at index 2
// 4 is at index 4

each(array2, each_callback);
// Returns
// 1 is at index 0
// 2 is at index 1
// NaN is at index 3
// Infinity is at index 4
// s is at index 5
// [object Object] is at index 6