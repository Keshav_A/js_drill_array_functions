import { items } from "../data.js";
import { find_callback } from "../find.js";
import { find } from "../find.js";

// find_callback function returns element, if true

// Test 1

let array = find({}, find_callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns "undefined"

// Test 2

array = find([], find_callback);
// Returns "Empty Array"
console.log(array);
// Returns "undefined"

// Test 3

array = find([]);
// Returns "Empty Array"
console.log(array);
// Returns "undefined"

// Test 4

array = find(find_callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns "undefined"

// Test 5

array = find(items);
// Returns "Provide callback function as an argument"
console.log(array);
// Returns "undefined"

// Test 6

array = find(items, "function");
// Returns "Provide callback function as an argument"
console.log(array);
// Returns "undefined"

// Test 7

array = find(items, find_callback);
console.log(array);
// Returns
// 5

// Test 8

array = find([1, 2, 3, "undefined", 4], find_callback);
console.log(array);
// Returns "Element not present"
