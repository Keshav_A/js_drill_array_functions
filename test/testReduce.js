import { items } from "../data.js";
import { reduce_callback } from "../reduce.js";
import { reduce } from "../reduce.js";

// Test1

let array = reduce({}, reduce_callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns Undefined

array = reduce([], reduce_callback);
// Returns "Empty Array"
console.log(array);
// Returns Undefined

array = reduce([]);
// Returns "Empty Array"
console.log(array);
// Returns Undefined

array = reduce(reduce_callback);
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns "Undefined"

array = reduce(items);
// Returns "Provide function as second argument"
console.log(array);
// Returns Undefined

array = reduce(items, "function");
// Provide function as second argument
console.log(array);
// Returns "Undefined"

array = reduce(items, reduce_callback);
console.log(array);
// Returns 20

array = reduce([1, 2, 3, "undefined", 4, 5], reduce_callback);
console.log(array);
// Returns "6undefined45"

array = reduce(items,reduce_callback,10)
console.log(array)