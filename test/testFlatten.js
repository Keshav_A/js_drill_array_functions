import { items } from "../data.js";
import { flatten } from "../flatten.js";

// Test 1

let array = flatten({});
// Returns "Not an array, Provide array as an argument"
console.log(array);
// Returns "undefined"

// Test 2

array = flatten([]);
// Returns "Empty Array"
console.log(array);
// Returns "undefined"

// Test 3

array = flatten([]);
// Returns "Empty Array"
console.log(array);
// Returns "undefined"


// Test 4

array = flatten(items);
console.log(array);
// Returns
// [1, 2, 3, 4, 5, 5]

// Test 5

array = flatten([1, 2, 3, "undefined", 4, 5]);
console.log(array);
// Returns
// [1, 2, 3, 'undefined', 4, 5]

// Test 6

array = flatten([1,2,3,[4,5,[6,7],8,[9,10],1,2,3],12,12,23,[1]])
console.log(array)
// Returns
// [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 12, 12, 23, 1]